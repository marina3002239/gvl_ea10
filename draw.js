// Canvas class to define a drawing area
class Canvas {
  #id;
  #parent;
  #gl;
  #width;
  #height;
  #canvas;

  constructor(id, parent) {
    this.#id = id;
    this.#parent = parent;
    this.#width = parent.clientWidth;
    this.#height = parent.clientHeight;

    let divWrapper = document.createElement("div");
    this.#canvas = document.createElement("canvas");
    this.#parent.appendChild(divWrapper);
    divWrapper.appendChild(this.#canvas);

    divWrapper.id = this.#id;
    this.#canvas.width = this.#width;
    this.#canvas.height = this.#height;

    /** @type {WebGL2RenderingContext} */
    this.#gl = this.#canvas.getContext("webgl2");
    this.#gl.enable(this.#gl.DEPTH_TEST);
    this.#gl.depthFunc(this.#gl.LEQUAL);
  }

  get GL() {
    return this.#gl;
  }
}

const mat4 = glMatrix.mat4;
const vec3 = glMatrix.vec3;
const quat = glMatrix.quat;

// Entity class, abstract class for basic model functions
class Entity {
  _Transformation;
  constructor() {
    this._Transformation = mat4.create();
  }

  translate(direction) {
    mat4.translate(this._Transformation, this._Transformation, direction);
  }

  resetTransform() {
    this._Transformation = mat4.create();
  }

  rotate(rotation) {
    const quaternion = quat.create();
    const rot = mat4.create();
    quat.fromEuler(quaternion, rotation[0], rotation[1], rotation[2]);
    mat4.fromQuat(rot, quaternion);
    mat4.multiply(this._Transformation, this._Transformation, rot);
  }

  scale(scale) {
    mat4.scale(this._Transformation, this._Transformation, scale);
  }

  uniformscale(factor) {
    this.scale([factor, factor, factor]);
  }

  transform(scale, rotation, translation) {
    this.scale(scale);
    this.rotate(rotation);
    this.translate(translation);
  }

  set Transformation(mat) {
    this._Transformation = mat;
  }

  get Transformation() {
    return this._Transformation;
  }

  set Position(position) {
    this._Transformation[12] = position[0];
    this._Transformation[13] = position[1];
    this._Transformation[14] = position[2];
  }

  get Position() {
    let position = vec3.create();
    mat4.getTranslation(position, this._Transformation);
    return position;
  }

  get Rotation() {
    let rotation = quat.create();
    mat4.getRotation(rotation, this._Transformation);
    return rotation;
  }

  get Scale() {
    let scale = vec3.create();
    mat4.getScaling(scale, this._Transformation);
    return scale;
  }
}

// Camera class extends entity class defines the camera 
// of the scene with camera parameters
class Camera extends Entity {
  #FOV;
  #zNear;
  #zFar;
  #position;
  #target;
  #aspectRatio;

  #projectionMatrix;
  #viewMatrix;

  constructor(resolution, FOV, zNear, zFar, position, target) {
    super();
    this.#aspectRatio = resolution[0] / resolution[1];
    this.#FOV = (FOV * Math.PI) / 180.0;
    this.#zNear = zNear;
    this.#zFar = zFar;
    this.Position = position;

    this.#projectionMatrix = mat4.create();
    mat4.perspective(
      this.#projectionMatrix,
      this.#FOV,
      this.#aspectRatio,
      this.#zNear,
      this.#zFar
    );
  }

  reset() {
    this.resetTransform();
    this.Position = [0, 0, 10];
  }

  get Projection() {
    return this.#projectionMatrix;
  }

  get View() {
    return this.Transformation;
  }

  get InverseView() {
    let inverse = mat4.create();
    mat4.invert(inverse, this.Transformation);
    return inverse;
  }

  get AspectRatio() {
    return this.#aspectRatio;
  }

  set Target(target) {
    this.#target = target;
  }
}

// Scene class defines the scene basic functions like drawing 
// functions and models processing functions
class Scene {
  #camera;
  #models;
  #lights;
  #ambientColor;

  constructor(glContext, ambientColor) {
    gl = glContext;
    const resolution = [gl.canvas.clientWidth, gl.canvas.clientHeight];
    this.#camera = new Camera(resolution, 45, 0.1, 100, [0, 0, 10], [0, 0, 0]);
    this.#models = [];
    this.#lights = [];
    this.#ambientColor = ambientColor;
  }

  get Lights() {
    return this.#lights;
  }
  get Models() {
    return this.#models;
  }

  get Camera() {
    return this.#camera;
  }

  get AmbientColor() {
    return this.#ambientColor;
  }

  addModel(model) {
    this.#models.push(model);
  }

  addLight(light) {
    this.#lights.push(light);
  }

  draw(time) {
    this.#clear();
    this.#models.forEach((model) => {
      model.draw(this, time);
    });
  }

  #clear() {
    gl.clearColor(
      this.#ambientColor[0],
      this.#ambientColor[1],
      this.#ambientColor[2],
      1.0
    );
    gl.clearDepth(1.0);
    gl.clear(gl.COLOR_BUFFER_BIT || gl.DEPTH_BUFFER_BIT);
  }
}

// Enum class for colors vectors to be used for shaders
class Colors {
  constructor() {}

  static White = [1.0, 1.0, 1.0];
  static Black = [0.0, 0.0, 0.0];
  static Red = [1.0, 0.0, 0.0];
  static Green = [0.0, 1.0, 0.0];
  static Blue = [0.0, 0.0, 1.0];
  static SkyBlue = [0.53, 0.8, 0.92];
  static SunLight = [1.0, 0.91, 0.7];
  static DarkGrey = [0.1, 0.1, 0.1];
  static DarkBlue = [0.04, 0.05, 0.08];
  static LightOrange = [0.968, 0.733, 0.505];
  static LightPurple = [0.564, 0.407, 0.968];
  static LightGreen = [0.309, 0.968, 0.376];
  static DarkPurple = [0.113, 0.062, 0.219];
}

// Material class, abstract class for material functions
class Material {
  #shader;
  #baseColor;

  constructor(glContext, shader, color) {
    gl = glContext;
    this.#shader = shader;
    this.#baseColor = color;
  }

  loadTexture(texture, path) {
    const image = new Image();
    if (path === null) {
      gl.bindTexture(gl.TEXTURE_2D, texture);
      gl.texImage2D(
        gl.TEXTURE_2D,
        0,
        gl.RGBA,
        1,
        1,
        0,
        gl.RGBA,
        gl.UNSIGNED_BYTE,
        new Uint8Array([255, 255, 255, 255])
      );
    } else {
      image.onload = function () {
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texImage2D(
          gl.TEXTURE_2D,
          0,
          gl.RGBA,
          gl.RGBA,
          gl.UNSIGNED_BYTE,
          this
        );
        gl.generateMipmap(gl.TEXTURE_2D);
      };
      image.src = path;
    }
  }

  get Shader() {
    return this.#shader;
  }

  get BaseColor() {
    return this.#baseColor;
  }
}

// Shader class to manage shader loading, compliation and execution
class Shader {
  #vertexShaderSource;
  #fragmentShaderSource;
  #program;

  constructor(glContext, shaderType) {
    gl = glContext;
    this.#vertexShaderSource = this.getShaderSource(shaderType[0]);
    this.#fragmentShaderSource = this.getShaderSource(shaderType[1]);
    this.#program = this.initShaderProgram(
      this.#vertexShaderSource,
      this.#fragmentShaderSource
    );
  }

  use() {
    gl.useProgram(this.#program);
  }

  setInt(uniformName, value) {
    const uniformID = gl.getUniformLocation(this.#program, uniformName);
    gl.uniform1i(uniformID, value);
  }

  setFloat(uniformName, value) {
    const uniformID = gl.getUniformLocation(this.#program, uniformName);
    gl.uniform1f(uniformID, value);
  }

  setVec3(uniformName, value) {
    const uniformID = gl.getUniformLocation(this.#program, uniformName);
    gl.uniform3fv(uniformID, value);
  }

  setVec4(uniformName, value) {
    const uniformID = gl.getUniformLocation(this.#program, uniformName);
    gl.uniform4fv(uniformID, value);
  }

  setMat4(uniformName, value) {
    const uniformID = gl.getUniformLocation(this.#program, uniformName);
    gl.uniformMatrix4fv(uniformID, false, value);
  }

  getUniformLocation(uniformName) {
    return gl.getUniformLocation(this.#program, uniformName);
  }

  getShaderSource(url) {
    let req = new XMLHttpRequest();
    req.open("GET", url, false);
    req.send();
    return req.status === 200 ? req.responseText : null;
  }

  loadShader(type, source) {
    const shader = gl.createShader(type);
    gl.shaderSource(shader, source);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
      alert(
        "An error occurred compiling the shaders: " +
          gl.getShaderInfoLog(shader)
      );
      gl.deleteShader(shader);
      return null;
    }
    return shader;
  }

  initShaderProgram(vsSource, fsSource) {
    const vertexShader = this.loadShader(gl.VERTEX_SHADER, vsSource);
    const fragmentShader = this.loadShader(gl.FRAGMENT_SHADER, fsSource);

    const shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
      alert(
        "Unable to initialize the shader program: " +
          gl.getProgramInfoLog(shaderProgram)
      );
      return null;
    }

    return shaderProgram;
  }
}

// Enum class for shaders to be used for shaders source code
class ShaderType {
  constructor() {}

  static Phong = ["Shaders/Phong.vs", "Shaders/Phong.fs"];
  static PBR = ["Shaders/PBR.vs", "Shaders/PBR.fs"];
  static Noise = ["Shaders/Noise.vs", "Shaders/Noise.fs"];
  static Constant = ["Shaders/Constant.vs", "Shaders/Constant.fs"];
}

// Constant Material class, extends the material class to implement 
// plane material
class ConstantMaterial extends Material {
  constructor(glContext, color) {
    super(glContext, new Shader(glContext, ShaderType.Constant), color);
  }
}

// Model bas class extends entity class to implement Model
// base functions
class ModelBase extends Entity {
  #material;
  #mesh;
  constructor(glContext, mesh, material) {
    super();
    gl = glContext;
    this.#mesh = mesh;
    this.#material = material;
  }

  draw(scene, time) {
    const shader = this.#material.Shader;
    const camera = scene.Camera;
    shader.use();
    // gl.bindTexture(gl.TEXTURE_2D, this.#material.ColorMap);

    const colorLocation = shader.getUniformLocation("u_ColorMap");
    const metalLocation = shader.getUniformLocation("u_MetalMap");
    const roughnessLocation = shader.getUniformLocation("u_RoughnessMap");
    const normalLocation = shader.getUniformLocation("u_NormalMap");

    gl.uniform1i(colorLocation, 0);
    gl.uniform1i(metalLocation, 1);
    gl.uniform1i(roughnessLocation, 2);
    gl.uniform1i(normalLocation, 3);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, this.#material.ColorMap);
    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, this.#material.MetalMap);
    gl.activeTexture(gl.TEXTURE2);
    gl.bindTexture(gl.TEXTURE_2D, this.#material.RoughnessMap);
    gl.activeTexture(gl.TEXTURE3);
    gl.bindTexture(gl.TEXTURE_2D, this.#material.NormalMap);

    scene.Lights.forEach((light, index) => {
      shader.setInt(`lights[${index}].type`, light.Type);
      shader.setVec3(`lights[${index}].position`, light.Position);
      shader.setVec3(`lights[${index}].color`, light.Color);
      shader.setFloat(`lights[${index}].strength`, light.Strength);
    });

    shader.setMat4("uProjection", camera.Projection);
    shader.setMat4("uView", camera.InverseView);
    shader.setVec3("objectColor", this.#material.BaseColor);
    shader.setVec3("ambientColor", scene.AmbientColor);
    shader.setInt("numberLights", scene.Lights.length);
    shader.setMat4("uWorld", this.Transformation);
    shader.setVec3("viewPosition", camera.Position);
    shader.setFloat("u_time", time);

    this.#mesh.draw();
  }
}

// Vertex class, abstract class for basic vertex functions
class Vertex {
  #position;
  #normal;
  #texCoord;
  constructor(position, normal, texCoord) {
    this.#position = position;
    this.#normal = normal;
    this.#texCoord = texCoord;
  }

  get Position() {
    return this.#position;
  }

  get Normal() {
    return this.#normal;
  }

  get TexCoord() {
    return this.#texCoord;
  }
}

// Mesh Base class, abstract class for basic mesh functions
// to build a mesh from the vertex list
class MeshBase {
  #vao;
  #vbo;
  #indexBuffer;
  #indices;

  constructor(glContext) {
    gl = glContext;
  }

  setupMesh(vertices, indices) {
    this.#indices = indices;
    this.#vao = gl.createVertexArray();
    this.#vbo = gl.createBuffer();
    this.#indexBuffer = gl.createBuffer();

    // 3 x 4bytes for position, 3 x 4bytes for normal, 2 x 4 bytes for textureCoord;
    const bytesPerVertex = 32;

    const buffer = new ArrayBuffer(bytesPerVertex * vertices.length);
    const dataView = new DataView(buffer);

    for (let i = 0; i < vertices.length; i++) {
      // console.log(i);
      dataView.setFloat32(bytesPerVertex * i, vertices[i].Position[0], true);
      dataView.setFloat32(
        bytesPerVertex * i + 4,
        vertices[i].Position[1],
        true
      );
      dataView.setFloat32(
        bytesPerVertex * i + 8,
        vertices[i].Position[2],
        true
      );

      dataView.setFloat32(bytesPerVertex * i + 12, vertices[i].Normal[0], true);
      dataView.setFloat32(bytesPerVertex * i + 16, vertices[i].Normal[1], true);
      dataView.setFloat32(bytesPerVertex * i + 20, vertices[i].Normal[2], true);

      dataView.setFloat32(
        bytesPerVertex * i + 24,
        vertices[i].TexCoord[0],
        true
      );
      dataView.setFloat32(
        bytesPerVertex * i + 28,
        vertices[i].TexCoord[1],
        true
      );
    }

    gl.bindVertexArray(this.#vao);

    gl.bindBuffer(gl.ARRAY_BUFFER, this.#vbo);
    gl.bufferData(gl.ARRAY_BUFFER, buffer, gl.STATIC_DRAW);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.#indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, this.#indices, gl.STATIC_DRAW);

    // vertex positions
    gl.enableVertexAttribArray(0);
    gl.vertexAttribPointer(0, 3, gl.FLOAT, false, bytesPerVertex, 0);

    // vertex normals
    gl.enableVertexAttribArray(1);
    gl.vertexAttribPointer(1, 3, gl.FLOAT, false, bytesPerVertex, 12);

    // vertex texture coordinates
    gl.enableVertexAttribArray(2);
    gl.vertexAttribPointer(2, 2, gl.FLOAT, false, bytesPerVertex, 24);

    gl.bindVertexArray(null);
  }

  draw() {
    gl.bindVertexArray(this.#vao);
    gl.drawElements(gl.TRIANGLES, this.#indices.length, gl.UNSIGNED_SHORT, 0);
    gl.bindVertexArray(null);
  }

  drawWireframe() {
    gl.bindVertexArray(this.#vao);
    gl.drawElements(gl.LINES, this.#indices.length, gl.UNSIGNED_SHORT, 0);
    gl.bindVertexArray(null);
  }
}

// Sphere Mesh class extends Mesh Base class to create a sphere mesh
class SphereMesh extends MeshBase {
  #vertices;
  #indices;
  #vertexData;
  constructor(glContext, resolution) {
    super(glContext);
    gl = glContext;
    this.#vertexData = this.createVertexData(resolution);
    this.#vertices = this.#vertexData.vertices;
    this.#indices = this.#vertexData.indices;
    this.setupMesh(this.#vertices, this.#indices);
  }

  createVertexData(resolution) {
    var n = resolution;
    var m = resolution;

    // Index data.
    var indicesLines = new Uint16Array(2 * 2 * n * m);
    var indicesTris = new Uint16Array(3 * 2 * n * m);

    var du = (2 * Math.PI) / n;
    var dv = Math.PI / m;
    var r = 1;
    // Counter for entries in index array.
    var iLines = 0;
    var iTris = 0;

    let vertices = [];
    // Loop angle u.
    for (var i = 0, u = 0; i <= n; i++, u += du) {
      // Loop angle v.
      for (var j = 0, v = 0; j <= m; j++, v += dv) {
        let position = [];
        let normal = [];
        let uv = [];

        uv.push(u);
        uv.push(v);

        let iVertex = i * (m + 1) + j;

        let x = r * Math.sin(v) * Math.cos(u);
        let y = r * Math.sin(v) * Math.sin(u);
        let z = r * Math.cos(v);

        // Set vertex positions.
        position.push(x);
        position.push(y);
        position.push(z);

        // Calc and set normals.
        let vertexLength = Math.sqrt(x * x + y * y + z * z);
        normal.push(x / vertexLength);
        normal.push(y / vertexLength);
        normal.push(z / vertexLength);
        normal.push(1.0);

        // Set index.
        // Line on beam.
        if (j > 0 && i > 0) {
          indicesLines[iLines++] = iVertex - 1;
          indicesLines[iLines++] = iVertex;
          // Line on ring.
          indicesLines[iLines++] = iVertex - (m + 1);
          indicesLines[iLines++] = iVertex;
          // Set index.
          // Two Triangles.
          indicesTris[iTris++] = iVertex;
          indicesTris[iTris++] = iVertex - 1;
          indicesTris[iTris++] = iVertex - (m + 1);
          indicesTris[iTris++] = iVertex - 1;
          indicesTris[iTris++] = iVertex - (m + 1) - 1;
          indicesTris[iTris++] = iVertex - (m + 1);
        }
        vertices.push(new Vertex(position, normal, uv));
      }
    }
    return {
      vertices: vertices,
      indices: indicesTris,
    };
  }
}

// Sphere Model class extends Model Base class to create a sphere model
// A Sphere model builds internally a sphere mesh class
class Sphere extends ModelBase {
  constructor(gl, resolution, material) {
    super(gl, new SphereMesh(gl, resolution), material);
  }
}

// CVS reader helper class
class CsvReader {
  static Read(url) {
    let req = new XMLHttpRequest();
    req.open("GET", url, false);
    req.send();
    let csvText = req.status === 200 ? req.responseText : null;
    if (csvText != null) {
      let result = [];

      let lines = csvText.split(/\r?\n/);
      lines.forEach((line) => {
        let data = line.split(",");
        let category = parseInt(data[data.length - 1]);
        data = data.slice(0, data.length - 1);
        data = data.map(Number);
        result.push({ category: category, data: data });
      });
      return result;
    } else {
      return null;
    }
  }
}

// t-SNE algorithm configuration parameters
const opt = {
  epsilon: 10,
  perplexity: 25,
  dim: 3,
};

// Current step of the t-SNE
let currentStep = document.getElementById("currentStep");

// Initialize the t-SNE algorithm
const tsne = new tsnejs.tSNE(opt);
// Load seeds data
const csvData = CsvReader.Read("seeds.csv");
const tsneData = [];
csvData.forEach((data) => {
  tsneData.push(data.data);
});

// draw the t-SNE data
tsne.initDataRaw(tsneData);

let step = 0;
for (let i = 0; i < 110; i++) {
  tsne.step();
  step++;
}
currentStep.innerText = step;
const result = tsne.getSolution();

let manual = false;

// Calculate the next t-SNE step
function CalcStep() {
  if (!manual) {
    manual = true;
    step = 0;
    tsne.initDataRaw(tsneData);
  }
  tsne.step();
  step += 1;
  currentStep.innerText = step;
  const newPositions = tsne.getSolution();
  for (let i = 0; i < scene.Models.length; i++) {
    scene.Models[i].Position = newPositions[i];
  }
}

const container = document.getElementById("renderer");
const canvas = new Canvas("glCanvas", container);
let gl = canvas.GL;

// Create Scene.
const scene = new Scene(gl, Colors.DarkPurple);

// Create Materials
const sunMat = new ConstantMaterial(gl, Colors.SunLight);
const blueMat = new ConstantMaterial(gl, Colors.SkyBlue);
const redMat = new ConstantMaterial(gl, Colors.Red);

const seedMats = [sunMat, blueMat, redMat];

result.forEach((position, index) => {
  const category = csvData[index].category;
  const point = new Sphere(gl, 16, seedMats[category - 1]);
  scene.addModel(point);
  point.Position = position;
  point.uniformscale(0.1);
});

const rotationSpeed = 3.0;
const translationSpeed = 0.5;
function processInput(e) {
  if (e.code === "KeyH") {
    scene.Camera.rotate([0, rotationSpeed, 0]);
  }
  if (e.code === "KeyK") {
    scene.Camera.rotate([0, -rotationSpeed, 0]);
  }
  if (e.code === "KeyU") {
    scene.Camera.rotate([rotationSpeed, 0, 0]);
  }
  if (e.code === "KeyJ") {
    scene.Camera.rotate([-rotationSpeed, 0, 0]);
  }
  if (e.code === "KeyW") {
    scene.Camera.translate([0, 0, -translationSpeed]);
  }
  if (e.code === "KeyS") {
    scene.Camera.translate([0, 0, translationSpeed]);
  }
  if (e.code === "KeyA") {
    scene.Camera.translate([-translationSpeed, 0, 0]);
  }
  if (e.code === "KeyD") {
    scene.Camera.translate([translationSpeed, 0, 0]);
  }
  if (e.code === "KeyQ") {
    scene.Camera.translate([0, translationSpeed, 0]);
  }
  if (e.code === "KeyE") {
    scene.Camera.translate([0, -translationSpeed, 0]);
  }
  if (e.code === "KeyR") {
    scene.Camera.reset();
  }
  if (e.code === "KeyT") {
    CalcStep();
  }
}
window.addEventListener("keydown", processInput);
let time = 0.0;
function draw() {
  scene.draw(time);
  time += 0.01;

  window.requestAnimationFrame(draw);
}

window.requestAnimationFrame(draw);
